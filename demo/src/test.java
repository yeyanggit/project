import java.util.HashMap;
import java.util.Map;

public class test {

    private static Map<String,Double> m=new HashMap<>();

    private static double max = 0;
    private static double yh = 0;

    public static void main(String[] args) {
        init();
        Map<String,Double> r= new HashMap<>();
        r.put("苹果",2.0);
        r.put("草莓",3.0);

        double re = cale(r);
        System.out.println("题目1: "+re);

        add("芒果",20.0);
        r.put("芒果",3.0);
        re = cale(r);
        System.out.println("题目2: "+re);

        dz("草莓",0.8);
        re = cale(r);
        System.out.println("题目3: "+re);

        yh(100,10);
        re = cale(r);
        System.out.println("题目4: "+re);
    }


    public static void init(){
        m.put("苹果",8.0);
        m.put("草莓",13.0);
    }

    public static void add(String name,double price){
        m.put(name,price);
    }

    public static void dz(String name,double zs){
        m.put(name,m.get(name)*zs);
    }

    public static void yh(double man,double j){
        max = man;
        yh = j;
    }

    public static Double cale(Map<String,Double> r){
        double sum=0;
        for (String k:
             r.keySet()) {
           sum+=r.get(k) * m.get(k);
        }
        if(max>0&&yh>0){
            if(sum>=max){
                sum-=yh;
            }
        }
        return sum;
    }

}
